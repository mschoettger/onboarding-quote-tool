package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.QuestionType;
import com.allstate.repositories.QuestionTypeRepo;

@Service
public class QuestionTypeService {

	@Autowired
	private QuestionTypeRepo questionTypeRepo;
	
	public void CreateOrUpdate(QuestionType questionType) {
		questionTypeRepo.save(questionType);
	}
	
	public Optional<QuestionType> FindById(Integer questionTypeId) {
		return questionTypeRepo.findById(questionTypeId);
	}
	
}
