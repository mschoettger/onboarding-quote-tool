package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.Policy;
import com.allstate.repositories.PolicyRepo;

@Service
public class PolicyService {
	
	@Autowired
	private PolicyRepo policyRepo;
	
	public void CreateOrUpdate(Policy policy) {
		policyRepo.save(policy);
	}
	
	public Optional<Policy> FindById(Integer policyId) {
		return policyRepo.findById(policyId);
	}

}
