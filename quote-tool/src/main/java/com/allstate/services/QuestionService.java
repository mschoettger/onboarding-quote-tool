package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.Question;
import com.allstate.repositories.QuestionRepo;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepo questionRepo;
	
	public void CreateOrUpdate(Question question) {
		questionRepo.save(question);
	}
	
	public Optional<Question> FindById(Integer questionId) {
		return questionRepo.findById(questionId);
	}
	
}
