package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.QuoteUser;
import com.allstate.repositories.QuoteUserRepo;

@Service
public class QuoteUserService {
	
	@Autowired
	private QuoteUserRepo quoteUserRepo;
	
	public void CreateOrUpdate(QuoteUser quoteUser) {
		quoteUserRepo.save(quoteUser);
	}
	
	public Optional<QuoteUser> FindById(Integer quoteUserId) {
		return quoteUserRepo.findById(quoteUserId);
	}

}
