package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.Coverage;
import com.allstate.repositories.CoverageRepo;

@Service
public class CoverageService {

	@Autowired
	private CoverageRepo coverageRepo;
	
	public void CreateOrUpdate(Coverage coverage) {
		coverageRepo.save(coverage);
	}
	
	public Optional<Coverage> FindById(Integer coverageId) {
		return coverageRepo.findById(coverageId);
	}
}
