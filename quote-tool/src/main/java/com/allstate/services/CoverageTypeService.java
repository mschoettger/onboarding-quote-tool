package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.CoverageType;
import com.allstate.repositories.CoverageTypeRepo;

@Service
public class CoverageTypeService {

	@Autowired
	private CoverageTypeRepo coverageTypeRepo;
	
	public void CreateOrUpdate(CoverageType coverageType) {
		coverageTypeRepo.save(coverageType);
	}
	
	public Optional<CoverageType> FindById(Integer coverageTypeId) {
		return coverageTypeRepo.findById(coverageTypeId);
	}
}
