package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.StatePolicyType;
import com.allstate.repositories.StatePolicyTypeRepo;

@Service
public class StatePolicyTypeService {

	@Autowired
	private StatePolicyTypeRepo statePolicyTypeRepo;
	
	public void CreateOrUpdate(StatePolicyType statePolicyType) {
		statePolicyTypeRepo.save(statePolicyType);
	}
	
	public Optional<StatePolicyType> FindById(Integer statePolicyTypeId) {
		return statePolicyTypeRepo.findById(statePolicyTypeId);
	}
	
}
