package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.PolicyIssued;
import com.allstate.repositories.PolicyIssuedRepo;

@Service
public class PolicyIssuedService {

	@Autowired
	private PolicyIssuedRepo policyIssuedRepo;
	
	public void CreateOrUpdate(PolicyIssued policyIssued) {
		policyIssuedRepo.save(policyIssued);
	}
	
	public Optional<PolicyIssued> FindById(Integer policyIssuedId) {
		return policyIssuedRepo.findById(policyIssuedId);
	}
	
}
