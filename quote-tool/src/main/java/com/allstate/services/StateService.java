package com.allstate.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.State;
import com.allstate.repositories.StateRepo;

@Service
public class StateService {

	@Autowired
	private StateRepo stateRepo;
	
	public void CreateOrUpdate(State state) {
		stateRepo.save(state);
	}
	
	public Optional<State> FindById(Integer stateId) {
		return stateRepo.findById(stateId);
	}
	
	public List<State> getAll() {
		return stateRepo.findAllByOrderByName();
	}
}
 