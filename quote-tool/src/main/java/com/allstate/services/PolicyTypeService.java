package com.allstate.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.allstate.models.PolicyType;
import com.allstate.repositories.PolicyTypeRepo;

@Service
public class PolicyTypeService {

	@Autowired
	private PolicyTypeRepo policyTypeRepo;
	
	public void CreateOrUpdate(PolicyType policyType) {
		policyTypeRepo.save(policyType);
	}
	
	public Optional<PolicyType> FindById(int policyTypeId) {
		return policyTypeRepo.findById(policyTypeId);
	}
	
}
