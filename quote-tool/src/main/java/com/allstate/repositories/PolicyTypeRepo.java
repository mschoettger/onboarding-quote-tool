package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.PolicyType;

public interface PolicyTypeRepo extends CrudRepository<PolicyType, Integer>{

}
