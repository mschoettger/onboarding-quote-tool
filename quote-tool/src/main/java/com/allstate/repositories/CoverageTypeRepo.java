package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.CoverageType;

public interface CoverageTypeRepo extends CrudRepository<CoverageType, Integer>{

}
