package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.QuestionType;

public interface QuestionTypeRepo extends CrudRepository<QuestionType, Integer>{
	
}
