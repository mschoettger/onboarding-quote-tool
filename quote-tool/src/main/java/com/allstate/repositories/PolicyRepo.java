package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.Policy;

public interface PolicyRepo extends CrudRepository<Policy, Integer>{

}
