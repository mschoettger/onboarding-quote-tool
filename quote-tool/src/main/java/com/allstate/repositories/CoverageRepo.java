package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.Coverage;

public interface CoverageRepo extends CrudRepository<Coverage, Integer>{

}
