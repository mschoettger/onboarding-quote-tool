package com.allstate.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.State;

public interface StateRepo extends CrudRepository<State, Integer>{
	public List<State> findAllByOrderByName();
}
