package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.PolicyIssued;

public interface PolicyIssuedRepo extends CrudRepository<PolicyIssued, Integer>{

}
