package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.Question;

public interface QuestionRepo extends CrudRepository<Question, Integer>{

}
