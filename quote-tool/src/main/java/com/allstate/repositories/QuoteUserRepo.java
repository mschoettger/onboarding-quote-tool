package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.QuoteUser;

public interface QuoteUserRepo extends CrudRepository<QuoteUser, Integer>{

}
