package com.allstate.repositories;

import org.springframework.data.repository.CrudRepository;

import com.allstate.models.StatePolicyType;

public interface StatePolicyTypeRepo extends CrudRepository<StatePolicyType, Integer>{

}
