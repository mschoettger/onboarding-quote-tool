package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.QuestionType;
import com.allstate.services.QuestionTypeService;

@RestController
@RequestMapping(path="/questionType")
public class QuestionTypeController {

	@Autowired
	private QuestionTypeService questionTypeService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdateQuestionType(@RequestBody QuestionType questionType) {
		questionTypeService.CreateOrUpdate(questionType);
	}
	
	@PostMapping(path="/findById")
	public ResponseEntity<QuestionType> getQuestionTypeById(@RequestBody QuestionType questionType){
		QuestionType questionTypeEntity = new QuestionType();
		try {
			questionTypeEntity = questionTypeService.FindById(questionType.getQuestionTypeId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<QuestionType>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<QuestionType>(questionTypeEntity, HttpStatus.OK);
	}
	
}
