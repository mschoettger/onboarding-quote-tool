package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.QuoteUser;
import com.allstate.services.QuoteUserService;

@RestController
@RequestMapping(path="/quoteUser")
public class QuoteUserController {

	@Autowired
	private QuoteUserService quoteUserService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdateQuoteUser(@RequestBody QuoteUser quoteUser) {
		quoteUserService.CreateOrUpdate(quoteUser);
	}
	
	@PostMapping(path="/findById")
	public ResponseEntity<QuoteUser> findQuoteUserById(@RequestBody QuoteUser quoteUser){
		QuoteUser quoteUserEntity = new QuoteUser();
		try {
			quoteUserEntity = quoteUserService.FindById(quoteUser.getQuoteUserId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<QuoteUser>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<QuoteUser>(quoteUserEntity, HttpStatus.OK);
	}
}
