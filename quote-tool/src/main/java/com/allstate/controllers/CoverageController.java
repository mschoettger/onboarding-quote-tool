package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.Coverage;
import com.allstate.services.CoverageService;

@RestController
@RequestMapping(path="/coverage")
public class CoverageController {
	
	@Autowired
	private CoverageService coverageService;
	
	@PostMapping(path="/createOrUpdate")
	public void CreateOrUpdateCoverage(@RequestBody Coverage coverage) {
		coverageService.CreateOrUpdate(coverage);
	}
	
	@GetMapping(path="/findById")
	public ResponseEntity<Coverage> GetCoverageById(@RequestBody Coverage coverage) {
		Coverage coverageEntity = new Coverage();
		try {
			coverageEntity = coverageService.FindById(coverage.getCoverageId()).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<Coverage>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Coverage>(coverageEntity, HttpStatus.OK);
	}
}
