package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.PolicyType;
import com.allstate.services.PolicyTypeService;

@RestController
@RequestMapping(path="/policyType")
public class PolicyTypeController {

	@Autowired
	private PolicyTypeService policyTypeService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdatePolicyType(@RequestBody PolicyType policyType) {
		policyTypeService.CreateOrUpdate(policyType);
	}
	
	@GetMapping(path="/findById")
	public ResponseEntity<PolicyType> GetPolicyTypeById(@RequestBody PolicyType policyType) {
		PolicyType policyTypeEntity = new PolicyType();
		try {
			policyTypeEntity = policyTypeService.FindById(policyType.getPolicyTypeId()).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<PolicyType>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<PolicyType>(policyTypeEntity, HttpStatus.OK);
	}
}
