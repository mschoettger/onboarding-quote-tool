package com.allstate.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.Policy;
import com.allstate.models.PolicyType;
import com.allstate.models.QuoteVm;
import com.allstate.services.PolicyTypeService;

@RestController
@RequestMapping(path="/quote")
public class QuoteVmController {

	@Autowired
	private PolicyTypeService policyTypeService;
	
	@PostMapping(path="/getQuotes")
	public ResponseEntity<List<Policy>> getPolicyListByPolicyTypeId(@RequestBody QuoteVm quoteVm){
		PolicyType policyType = new PolicyType();
		try {
			policyType = policyTypeService.FindById(quoteVm.getPolicyType().getPolicyTypeId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<List<Policy>>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<List<Policy>>(policyType.getPolicyList(), HttpStatus.OK);
	}
	
}
