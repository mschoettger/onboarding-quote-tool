package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.PolicyIssued;
import com.allstate.services.PolicyIssuedService;

@RestController
@RequestMapping(path="/policyIssued")
public class PolicyIssuedController {

	@Autowired
	private PolicyIssuedService policyIssuedService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdatePolicyIssued(@RequestBody PolicyIssued policyIssued) {
		policyIssuedService.CreateOrUpdate(policyIssued);
	}
	
	@GetMapping(path="/findById")
	public ResponseEntity<PolicyIssued> GetCoverageById(@RequestBody PolicyIssued policyIssued) {
		PolicyIssued policyIssuedEntity = new PolicyIssued();
		try {
			policyIssuedEntity = policyIssuedService.FindById(policyIssued.getPolicyIssuedId()).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<PolicyIssued>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<PolicyIssued>(policyIssuedEntity, HttpStatus.OK);
	}
}
