package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.CoverageType;
import com.allstate.services.CoverageTypeService;

@RestController
@RequestMapping(path="/coverageType")
public class CoverageTypeController {

	@Autowired
	private CoverageTypeService coverageTypeService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdatePolicyType(@RequestBody CoverageType coverageType) {
		coverageTypeService.CreateOrUpdate(coverageType);
	}
	
	@GetMapping(path="/findById")
	public ResponseEntity<CoverageType> GetCoverageTypeById(@RequestBody CoverageType coverageType) {
		CoverageType coverageTypeEntity = new CoverageType();
		try {
			coverageTypeEntity = coverageTypeService.FindById(coverageType.getCoverageTypeId()).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<CoverageType>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<CoverageType>(coverageTypeEntity, HttpStatus.OK);
	}
}

