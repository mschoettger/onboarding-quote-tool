package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.Question;
import com.allstate.services.QuestionService;

@RestController
@RequestMapping(path="/question")
public class QuestionController {
	
	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdateQuestion(@RequestBody Question question) {
		questionService.CreateOrUpdate(question);
	}
	
	@PostMapping(path="/findById")
	public ResponseEntity<Question> findQuestionById(@RequestBody Question question){
		Question questionEntity = new Question();
		try {
			questionEntity = questionService.FindById(question.getQuestionId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<Question>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Question>(questionEntity, HttpStatus.OK);
	}
}