package com.allstate.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.State;
import com.allstate.services.StateService;

@RestController
@RequestMapping(path="/state")
@CrossOrigin
public class StateController {
	
	@Autowired
	private StateService stateService;
	
	@PostMapping(path="/createOrUpdate")
	public void CreateOrUpdateState(@RequestBody State state) {
		stateService.CreateOrUpdate(state);
	}
	
	@PostMapping(path="/findById")
	public ResponseEntity<State> FindStateById(@RequestBody State state){
		State stateEntity = new State();
		try {
			stateEntity = stateService.FindById(state.getStateId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<State>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<State>(stateEntity, HttpStatus.OK);
	}
	
	@GetMapping(path="/all")
	public ResponseEntity<List<State>> GetAllStates() {
		return new ResponseEntity<List<State>>(stateService.getAll(), HttpStatus.OK);
	}
}
