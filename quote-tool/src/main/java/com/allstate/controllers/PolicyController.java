package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.Policy;
import com.allstate.services.PolicyService;

@RestController
@RequestMapping(path="/policy")
public class PolicyController {
	
	@Autowired
	private PolicyService policyService;
	
	@RequestMapping(path="/createOrUpdate")
	public void CreateOrUpdatePolicy(@RequestBody Policy policy) {
		policyService.CreateOrUpdate(policy);
	}
	
	@GetMapping(path="/findById")
	public ResponseEntity<Policy> GetPolicyById(@RequestBody Policy policy) {
		Policy policyEntity = new Policy();
		try {
			policyEntity = policyService.FindById(policy.getPolicyId()).get();
		} catch (NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<Policy>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Policy>(policyEntity, HttpStatus.OK);
	}
}
