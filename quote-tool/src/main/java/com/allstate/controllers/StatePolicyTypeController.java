package com.allstate.controllers;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.allstate.models.StatePolicyType;
import com.allstate.services.StatePolicyTypeService;

@RestController
@RequestMapping(path="/statePolicyType")
@CrossOrigin
public class StatePolicyTypeController {
	
	@Autowired
	private StatePolicyTypeService statePolicyTypeService;
	
	@PostMapping(path="/createOrUpdate")
	public void CreateOrUpdateState(@RequestBody StatePolicyType statePolicyType) {
		statePolicyTypeService.CreateOrUpdate(statePolicyType);
	}
	
	@PostMapping(path="/findById")
	public ResponseEntity<StatePolicyType> FindStatePolicyType(@RequestBody StatePolicyType statePolicyType){
		StatePolicyType statePolicyTypeEntity = new StatePolicyType();
		try {
			statePolicyTypeEntity = statePolicyTypeService.FindById(statePolicyType.getStatePolicyTypeId()).get();
		} catch(NoSuchElementException e) {
			e.printStackTrace();
			return new ResponseEntity<StatePolicyType>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<StatePolicyType>(statePolicyTypeEntity, HttpStatus.OK);
	}
	
}
