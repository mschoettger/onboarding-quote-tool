package com.allstate.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class PolicyType {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int policyTypeId;
	
	@Column
	private String name;
	
	//One-to-Many: One Policy Type has many Policies
	@OneToMany(mappedBy="policyType")
	private List<Policy> policyList;
	
	//One-to-Many: One Policy Type has many Questions
	@OneToMany(mappedBy="policyType")
	private List<Question> questionList;

	public int getPolicyTypeId() {
		return policyTypeId;
	}
	
	public void setPolicyTypeId(int policyTypeId) {
		this.policyTypeId = policyTypeId;
	}

	public void setId(int policyTypeId) {
		this.policyTypeId = policyTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public List<Policy> getPolicyList() {
		return policyList;
	}

	public void setPolicyList(List<Policy> policyList) {
		this.policyList = policyList;
	}

	public List<Question> getQuestionList() {
		return questionList;
	}

	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}

	@Override
	public String toString() {
		return "PolicyType [policyTypeId=" + policyTypeId + ", name=" + name + "]";
	}

}
