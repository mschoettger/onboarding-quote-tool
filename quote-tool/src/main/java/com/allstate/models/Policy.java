package com.allstate.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Policy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int policyId;
	
	//Many-to-One: Many Policies belong to one Policy Type
	@ManyToOne
	@JoinColumn(name="policyTypeId")
	private PolicyType policyType;
	
	@Column
	private String name;
	
	//One-to-Many: One Policy can have many Policies Issued
	@OneToMany(mappedBy="policyIssuedId")
	private List<PolicyIssued> policyIssuedList;
	
	//One-to-Many: One Policy can have many Coverages
	@OneToMany(mappedBy="coverageId")
	private List<Coverage> coverageList;

	public int getPolicyId() {
		return policyId;
	}

	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}

	public PolicyType getPolicyType() {
		return policyType;
	}

	public void setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Policy [policyId=" + policyId + ", policyType=" + policyType + ", name=" + name + "]";
	}

}
