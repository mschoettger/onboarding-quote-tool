package com.allstate.models;

public class QuoteVm {

	private QuoteUser quoteUser;
	private PolicyType PolicyType;
	private boolean ownProperty;
	private int squareFeet;
	private int monthlyPayment;
	private boolean floodZone;
	private float propertyValue;
	
	public QuoteUser getQuoteUser() {
		return quoteUser;
	}
	public void setQuoteUser(QuoteUser quoteUser) {
		this.quoteUser = quoteUser;
	}
	public PolicyType getPolicyType() {
		return PolicyType;
	}
	public void setPolicyType(PolicyType policyType) {
		PolicyType = policyType;
	}
	public boolean isOwnProperty() {
		return ownProperty;
	}
	public void setOwnProperty(boolean ownProperty) {
		this.ownProperty = ownProperty;
	}
	public int getSquareFeet() {
		return squareFeet;
	}
	public void setSquareFeet(int squareFeet) {
		this.squareFeet = squareFeet;
	}
	public int getMonthlyPayment() {
		return monthlyPayment;
	}
	public void setMonthlyPayment(int monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}
	public boolean isFloodZone() {
		return floodZone;
	}
	public void setFloodZone(boolean floodZone) {
		this.floodZone = floodZone;
	}
	public float getPropertyValue() {
		return propertyValue;
	}
	public void setPropertyValue(float propertyValue) {
		this.propertyValue = propertyValue;
	}
	
	@Override
	public String toString() {
		return "QuoteVM [quoteUser=" + quoteUser + ", PolicyType=" + PolicyType + ", ownProperty=" + ownProperty
				+ ", squareFeet=" + squareFeet + ", monthlyPayment=" + monthlyPayment + ", floodZone=" + floodZone
				+ ", propertyValue=" + propertyValue + "]";
	}

}
