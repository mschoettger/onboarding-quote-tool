package com.allstate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Question {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int questionId;
	
	//Many-to-One: Many Questions belong to one Question Type
	@ManyToOne
	@JoinColumn(name="questionTypeId")
	private QuestionType questionType;
	
	//Many-to-One: Many Questions belong to one Policy Type
	@ManyToOne
	@JoinColumn(name="policyTypeId")
	private PolicyType policyType;
	
	@Column
	private String fieldLabel;

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public PolicyType getPolicyType() {
		return policyType;
	}

	public void setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", questionType=" + questionType + ", policyType=" + policyType + ", fieldLabel="
				+ fieldLabel + "]";
	}

}
