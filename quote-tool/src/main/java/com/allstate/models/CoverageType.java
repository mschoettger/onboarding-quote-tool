package com.allstate.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class CoverageType {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int coverageTypeId;
	
	@Column
	private String name;
	
	//One-to-Many: One Policy Type has many Policies
	@OneToMany(mappedBy="coverageType")
	private List<Coverage> coverageList;

	public int getCoverageTypeId() {
		return coverageTypeId;
	}

	public void setCoverageTypeId(int coverageTypeId) {
		this.coverageTypeId = coverageTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Coverage> getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List<Coverage> coverageList) {
		this.coverageList = coverageList;
	}

	@Override
	public String toString() {
		return "CoverageType [coverageTypeId=" + coverageTypeId + ", name=" + name + ", coverageList=" + coverageList
				+ "]";
	}

}
