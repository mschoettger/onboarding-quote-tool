package com.allstate.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class QuestionType {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int questionTypeId;
	
	@Column
	private String questionType;
	
	//One-to-Many: One Question Type has many Questions
	@OneToMany(mappedBy="questionId")
	private List<Question> questionList;

	public int getQuestionTypeId() {
		return questionTypeId;
	}

	public void setQuestionTypeId(int questionTypeId) {
		this.questionTypeId = questionTypeId;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	@Override
	public String toString() {
		return "QuestionType [questionTypeId=" + questionTypeId + ", questionType=" + questionType + "]";
	}

}
