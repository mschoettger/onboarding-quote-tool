package com.allstate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class Coverage {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int coverageId;
	
	//Many-to-One: Many Coverages are associated with one Policy
	@ManyToOne
	@JoinColumn(name = "policyId")
	private Policy policy;
	
	@ManyToOne
	@JoinColumn(name = "coverageTypeId")
	private CoverageType coverageType;
	
	@Column
	private float limit;

	public int getCoverageId() {
		return coverageId;
	}

	public void setCoverageId(int coverageId) {
		this.coverageId = coverageId;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	public CoverageType getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(CoverageType coverageType) {
		this.coverageType = coverageType;
	}

	public float getLimit() {
		return limit;
	}

	public void setLimit(float limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "Coverage [coverageId=" + coverageId + ", policy=" + policy + ", coverageType=" + coverageType
				+ ", limit=" + limit + "]";
	}
	
}
