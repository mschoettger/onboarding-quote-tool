package com.allstate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class StatePolicyType {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int statePolicyTypeId;
	
	@ManyToOne
	@JoinColumn(name="stateId")
	private State state;
	
	@ManyToOne
	@JoinColumn(name="policyTypeId")
	private PolicyType policyType;
	
	@Column
	private float baseRate;

	public int getStatePolicyTypeId() {
		return statePolicyTypeId;
	}

	public void setStatePolicyTypeId(int statePolicyTypeId) {
		this.statePolicyTypeId = statePolicyTypeId;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public PolicyType getPolicyType() {
		return policyType;
	}

	public void setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
	}

	public float getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(float baseRate) {
		this.baseRate = baseRate;
	}

	@Override
	public String toString() {
		return "StatePolicyType [statePolicyTypeId=" + statePolicyTypeId + ", state=" + state + ", policyType="
				+ policyType + ", baseRate=" + baseRate + "]";
	}
	
	
}
