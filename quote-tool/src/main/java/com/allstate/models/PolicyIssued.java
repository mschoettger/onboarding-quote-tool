package com.allstate.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.stereotype.Component;

@Entity
@Component
public class PolicyIssued {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int policyIssuedId;
	
	//Many-to-One: Many Policies Issued belong to one User
	@ManyToOne
	@JoinColumn(name = "userId")
	private QuoteUser quoteUser;
	
	//Many-to-One: Many Policies Issued belong to one Policy
	@ManyToOne
	@JoinColumn(name = "policyId")
	private Policy policy;

	public int getPolicyIssuedId() {
		return policyIssuedId;
	}

	public void setPolicyIssuedId(int policyIssuedId) {
		this.policyIssuedId = policyIssuedId;
	}

	public QuoteUser getQuoteUser() {
		return quoteUser;
	}

	public void setUser(QuoteUser user) {
		this.quoteUser = user;
	}

	public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}

	@Override
	public String toString() {
		return "PolicyIssued [policyIssuedId=" + policyIssuedId + ", quoteUser=" + quoteUser + ", policy=" + policy + "]";
	}

}
