package com.allstate.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.stereotype.Component;

@Entity
@Component
public class State {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int stateId;
	
	@Column
	private String name;
	
	@Column
	private String abbreviation;
	
	//One-to-Many: 1 State has many Users
	@OneToMany(mappedBy = "state")
	private List<QuoteUser> quoteUserList;

	public int getStateId() {
		return stateId;
	}

	public void setStateId(int id) {
		this.stateId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@Override
	public String toString() {
		return "State [stateId=" + stateId + ", name=" + name + ", abbreviation=" + abbreviation + "]";
	}

}
