import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuoteFormComponent } from './quote-form/quote-form.component';
import { PolicyOptionsComponent } from './policy-options/policy-options.component';

const routes: Routes = [
  { path: '', redirectTo: '/quoteForm', pathMatch: 'full' },
  { path: 'quoteForm', component: QuoteFormComponent },
  { path: 'policyOptions', component: PolicyOptionsComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
