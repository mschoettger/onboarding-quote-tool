import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import * as $ from 'jquery';

import { AppComponent } from './app.component';
import { QuoteFormComponent } from './quote-form/quote-form.component';
import { AppRoutingModule } from './/app-routing.module';
import { PolicyOptionsComponent } from './policy-options/policy-options.component';
import { QuoteUserService } from './quote-user.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    QuoteFormComponent,
    PolicyOptionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [QuoteUserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
