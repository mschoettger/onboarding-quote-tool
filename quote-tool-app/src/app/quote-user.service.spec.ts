import { TestBed, inject } from '@angular/core/testing';

import { QuoteUserService } from './quote-user.service';

describe('QuoteUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuoteUserService]
    });
  });

  it('should be created', inject([QuoteUserService], (service: QuoteUserService) => {
    expect(service).toBeTruthy();
  }));
});
