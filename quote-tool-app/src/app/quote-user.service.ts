import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { State } from './models/State';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class QuoteUserService {

  constructor(private http: HttpClient) { }

  getAllStates(): Observable<State[]> {
    return this.http.get<State[]>('http://localhost:17381/state/all');
  }

}
