import { QuoteUser } from './QuoteUser';
import { Policy } from './Policy';

export class PolicyIssued {

    policyIssuedId: number;
    quoteUser: QuoteUser;
    policy: Policy;

    constructor() {
        this.policyIssuedId = 0;
        this.quoteUser = new QuoteUser();
        this.policy = new Policy();
    }

}
