import { QuoteUser } from './QuoteUser';
import { PolicyType } from './policyType';

export class QuoteVM {

    quoteUser: QuoteUser;
    policyType: PolicyType;
    ownProperty: boolean;
    squareFeet: number;
    monthlyPayment: number;
    floodZone: boolean;
    propertyValue: number;

    constructor() {
        this.quoteUser = new QuoteUser();
        this.policyType = new PolicyType();
        this.ownProperty = false;
        this.squareFeet = 0;
        this.monthlyPayment = 0;
        this.floodZone = false;
        this.propertyValue = 0;
    }

}
