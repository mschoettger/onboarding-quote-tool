import { PolicyType } from './policyType';
import { Coverage } from './coverage';
import { PolicyIssued } from './PolicyIssued';

export class Policy {

    policyId: number;
    policyType: PolicyType;
    name: String;
    policyIssuedList: Array<PolicyIssued>;
    coverageList: Array<Coverage>;

    constructor() {
        this.policyId = 0;
        this.policyType = new PolicyType();
        this.name = '';
        this.policyIssuedList = new Array<PolicyIssued>();
        this.coverageList = new Array<Coverage>();
    }

}
