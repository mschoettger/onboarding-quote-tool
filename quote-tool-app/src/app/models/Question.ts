import { PolicyType } from './PolicyType';
import { QuestionType } from './QuestionType';

export class Question {

    questionId: number;
    questionType: QuestionType;
    policyType: PolicyType;
    fieldLabel: String;

    constructor() {
        this.questionId = 0;
        this.questionType = new QuestionType();
        this.policyType = new PolicyType();
        this.fieldLabel = '';
    }

}
