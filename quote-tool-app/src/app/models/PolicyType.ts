import { Policy } from './policy';
import { Question } from './Question';

export class PolicyType {

    policyTypeId: number;
    name: String;
    policyList: Array<Policy>;
    questionList: Array<Question>;

    constructor() {
        this.policyTypeId = 0;
        this.name = '';
        this.policyList = new Array<Policy>();
        this.questionList = new Array<Question>();
    }

}
