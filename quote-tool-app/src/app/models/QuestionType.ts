import { Question } from './Question';

export class QuestionType {

    questionTypeId: number;
    name: String;
    questionList: Array<Question>;

    constructor () {
        this.questionTypeId = 0;
        this.name = '';
        this.questionList = new Array<Question>();
    }

}
