import { QuoteUser } from './QuoteUser';

export class State {

    stateId: number;
    name: String;
    abbreviation: String;
    quoteUserList: Array<QuoteUser>;

    constructor () {
        this.stateId = 0;
        this.name = '';
        this.abbreviation = '';
        this.quoteUserList = new Array<QuoteUser>();
    }

}
