import { Policy } from './policy';

export class Coverage {

    coverageId: number;
    policy: Policy;
    name: String;
    limit: number;

    constructor() {
        this.coverageId = 0;
        this.policy = new Policy();
        this.name = '';
        this.limit = 0;
    }

}
