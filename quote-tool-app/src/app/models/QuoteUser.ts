import { State } from './State';
import { PolicyIssued } from './PolicyIssued';

export class QuoteUser {

    quoteUserId: number;
    firstName: String;
    middleName: String;
    lastName: String;
    gender: String;
    dateOfBirth: String;
    address1: String;
    address2: String;
    city: String;
    state: State;
    zipCode: String;
    phoneNumber: String;
    email: String;
    policyIssuedList: Array<PolicyIssued>;

    constructor() {
        this.quoteUserId = 0;
        this.firstName = '';
        this.middleName = '';
        this.lastName = '';
        this.gender = 'm';
        this.dateOfBirth = '01-01-1970';
        this.address1 = '';
        this.address2 = '';
        this.city = '';
        this.state = new State();
        this.zipCode = '';
        this.phoneNumber = '';
        this.email = '';
        this.policyIssuedList = new Array<PolicyIssued>();
    }

}
