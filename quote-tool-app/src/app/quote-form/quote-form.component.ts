import { Component, OnInit } from '@angular/core';
import { QuoteUserService } from '../quote-user.service';
import { State } from '../models/State';
import { QuoteUser } from '../models/QuoteUser';
import { QuoteVM } from '../models/QuoteVM';

@Component({
  selector: 'app-quote-form',
  templateUrl: './quote-form.component.html',
  styleUrls: ['./quote-form.component.css']
})
export class QuoteFormComponent implements OnInit {

  private quoteVm: QuoteVM;
  private states: Array<State>;
  constructor(private quoteUserService: QuoteUserService) { }

  ngOnInit() {
    this.quoteVm = new QuoteVM();
    this.quoteUserService.getAllStates().subscribe(
      res => this.states = res
    );
    console.log(this.quoteVm);
  }

}
